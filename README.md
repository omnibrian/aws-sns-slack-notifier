# AWS SNS Slack Notifier

This project is a quick way to set up an SNS topic with a subscribed lambda function that will send messages published to the topic to slack.

## Setup

1. Navigate to https://{ team-domain }.slack.com/apps/manage/custom-integrations
2. Click to configure Incoming WebHooks
3. Add Configuration
4. Choose default channel for incoming messages
5. Copy webhook url into the serverless.yaml:13 file (It should look like https://hooks.slack.com/services/{ stuff })

## Deployment

Serverless will deploy a CloudFormation stack in the us-east-1 region creating an SNS topic, a lambda function, and an S3 bucket for storing lambda function versions. By default a stage name of 'prod' is used unless otherwise specified by the `--stage` option.

1. Install serverless globally: `yarn install --global serverless`
2. Install dependencies: `yarn install`
3. Make sure to have completed setup instructions
4. Deploy using serverless: `serverless deploy`

## Cleanup

To clean up and remove the resources created by this project, use: `serverless remove` (don't forget to specify `--stage` option if one was used during creation).
